# Changelog

- Added laptop role
- Added ssh server role
- Added wireguard server role
- Use wireguard.server.address_cidr variable to setup nat rule
- Add k3s roles
- Improve documentation
- Fix missing k3s.node.extra_agent_args in example.yaml args
- Suggest to disable traefik
