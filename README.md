# Kitchen Sink Playbook

![Kitchen Sink Setup](doc/img/kitchen-sink-setup.png)

This is a little playbook I'm using to configure old laptops and computers into a useful ([k3s](https://k3s.io/)) kubernetes cluster.  
It features a ([Wireguard](https://www.wireguard.com/)) VPN tunnel, in the unlikely scenario where I leave my house.  

## Prerequisites

1. All hosts run Debian 11
1. All hosts have the same sudoer user configured
1. All hosts have a ssh server running

## Roles

### SSH Server

This role will harden the ssh server setup, and add the desired user's authorized_keys.  
You can copy the public keys you desire into the ```roles/ssh_server/files/public_keys``` folder.  

### Wireguard server

This role will setup and start a Wireguard server.  
**NOTE:** As part of the configuration IP forwarding (routing) and SNAT are also enabled.  

### k3s

This is a work-for-me-simplification of [k3s-ansible](https://github.com/k3s-io/k3s-ansible).  
Two roles are defined: ```control_plane``` and ```node```, to form a [k3s](https://k3s.io/) kubernetes cluster.  

Once a control plane host is configured, you can copy the ```~/.kube/config``` file from the host to setup kubectl on other clients.  

## How to run the playbook

### Dry run

Do include the ```--check``` argument to execute a dry run.  

```sh
ansible-playbook playbook.yaml -e @vars/example.yaml -i hosts.example --ask-become-pass --check
```

### First run

When running the playbook on one or more hosts for the first time, include the ```--ask-pass``` argument, to authenticate to the host ssh server using a password.  

```sh
ansible-playbook playbook.yaml -e @vars/example.yaml -i hosts.example --ask-pass --ask-become-pass
```

### Adding new hosts (with no sshd key setup)

When adding new hosts that have not been configured yet, you will face mixed password authentication and key authentication ssh servers.  
To avoid errors, set the environment variable ```ANSIBLE_HOST_KEY_CHECKING=False``` to configure the new hosts for the first time.  

```sh
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yaml -e @vars/example.yaml -i hosts.example --ask-pass --ask-become-pass
```

### Other runs

```sh
ansible-playbook playbook.yaml -e @vars/example.yaml -i hosts.example --ask-become-pass
```
